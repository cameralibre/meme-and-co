import type RedditPost from './types/RedditPost'

const amIDoingThisRight: RedditPost = {
  id: 'uln1a4',
  subreddit: 'Wellington',
  userName: 'kiwibloke',
  avatar: '8d709ec3-8f88-4e15-99f1-4302153f0ee1.png',
  postUrl: 'https://www.reddit.com/r/Wellington/comments/uln1a4/am_i_doing_this_right',
  text: 'Am I doing this right?',
  image: 'NCj6K6Zl.jpeg',
  viewBox: '15 0 75 75', // SVG Viewbox, for centering/cropping the image
  time: '9:06 pm, 9 May 2022', // should eventually be Date
  upvotes: 92,
  comments: 10,
}

export default [
  amIDoingThisRight,
]