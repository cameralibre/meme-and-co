import type { Ref } from 'vue'
import Perspective from 'perspectivets'

import type Billboard from '@/types/Billboard'
import type ColorPair from '@/types/ColorPair'
import type TextStyle from '@/types/TextStyle'


export default function buildDraw (
  config: Billboard,
  photo: Ref<HTMLImageElement>,
  text: Ref<string>,
  fontSize: Ref<number>,
  fontWeight: Ref<number>,
  textCtx: CanvasRenderingContext2D,
  projectedCtx: CanvasRenderingContext2D,
  compCtx: CanvasRenderingContext2D
) {

  return function draw () {
    drawText(config, textCtx, text.value, fontSize.value, fontWeight.value)
    projectText(config, projectedCtx, textCtx)
    composite(config, compCtx, projectedCtx, photo)
  }
}

function drawText (config: Billboard, ctx: CanvasRenderingContext2D, text: string, size: number, weight: number) {
  // TODO should we pass in the canvas + ctx?
  // ctx clear
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height) // can we get dimensions from ctx?
  ctx.font = `${weight} ${size}px Montserrat`
  ctx.textBaseline = 'top'

  const textLines = text.split('\n')
  const measurements = ctx.measureText('Myg')
  const lineHeight = (measurements.actualBoundingBoxAscent + measurements.actualBoundingBoxDescent) * config.lineSpacing
  const totalHeight = textLines.length * lineHeight
  
  const anchorX = ctx.canvas.width * config.origin[0]
  const anchorY = ctx.canvas.height * config.origin[1]
  let startY = (config.alignVertical === 'center') ? anchorY - totalHeight / 2 : anchorY

  textLines.forEach(line => {
    const lineWidth = ctx.measureText(line.split('*').join('')).width
    const startX = (config.alignHorizontal === 'center') ? anchorX - lineWidth / 2 : anchorX
    fillText(ctx, splitByColor(line, config.colors), startX, startY)
    startY = startY + lineHeight
  })
}

function projectText (config: Billboard, targetCtx: CanvasRenderingContext2D, srcCtx: CanvasRenderingContext2D) {
  targetCtx.clearRect(0, 0, targetCtx.canvas.width, targetCtx.canvas.height)

  // @ts-ignore - wants 2nd arg to be HTMLImageElement, but passing in an HTMLCanvasElement works fine
  const perspective = new Perspective(targetCtx, srcCtx.canvas)
  perspective.draw(config.targetShape)
}

function composite (config: Billboard, ctx: CanvasRenderingContext2D, textCtx: CanvasRenderingContext2D, photo: any) {
  const image = photo.value as HTMLImageElement
  ctx.drawImage(image, 0, 0)

  ctx.drawImage(textCtx.canvas, 0, 0)
} 

function fillText (ctx: CanvasRenderingContext2D, colorTexts: TextStyle[], x: number, y: number) {
  colorTexts.forEach(({ text, fillStyle }) => {
    ctx.fillStyle = fillStyle
    ctx.fillText(text, x, y)
    x += ctx.measureText(text).width
  })
}

function splitByColor (text: string, colors: ColorPair) {
  let fragment = ''
  let isRed = false
  const output: TextStyle[] = []
  const colorText = (fragment: string, isRed: boolean) => ({ 
    text: (fragment) ? fragment : '', 
    fillStyle: (isRed) ? colors.red : colors.white 
  })

  text.split('').forEach((char, index, arr) => {
    if (char === '*') {
      if (fragment.length) {
        output.push(colorText(fragment, isRed))
        fragment = ''
      }
      isRed = !isRed
      return
    }

    fragment += char

    if (index === arr.length - 1) {
      output.push(colorText(fragment, isRed))
    }
  })

  return output 
}
