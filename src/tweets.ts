import type Tweet from './types/Tweet'

// export const residents: Tweet = {
//   id: '1523615662845104130',
//   avatar: 'BWXz33ji_400x400.png',
//   displayName: 'Orange Boy',
//   userName: 'PhilBStewart',
//   text: '',
//   url: '',
//   hashtag: '#HowLoweCanYouGo',
//   mention: '',
//   image: 'FST4XG2VgAAmJzr.jpeg',
//   viewBox: '15 0 75 75',
//   time: '10:47 PM, May 9, 2022',
//   likes: 10,
//   retweets: 3,
// }

export const weOwnNewtown: Tweet = {
  id: '1523867529957224448',
  avatar: 'K3YCx3LP_400x400.jpeg',
  displayName: 'Mister Grumpy',
  userName: 'GrumpyAnarchist',
  text: 'this is fun ',
  url: 'https://loweand.co.nz/billboard/1',
  hashtag: '',
  mention: '',
  image: 'FSXdPmXaIAIZoK4.jpeg',
  viewBox: '15 0 75 75',
  time: '3:28 PM, May 10, 2022',
  likes: 5,
  retweets: 1,
}

export const opportunity: Tweet = {
  id: '1523628837707206656',
  avatar: '9RDIAFXL_400x400.jpg',
  displayName: 'Keava',
  userName: 'Keava_Rose',
  text: '',
  url: 'https://loweand.co.nz',
  hashtag: '',
  mention: '',
  image: 'FSUEWmHUUAELAoO.jpeg',
  viewBox: '15 0 75 75',
  time: '11:39 pm, 9 May 2022',
  likes: 13,
  retweets: 4,
}

export const flatmates: Tweet = {
  id: '1523565301094559745',
  avatar: 'qnCkv3rk_400x400.jpg',
  displayName: 'Ian 🇳🇿🥝',
  userName: 'coolian2',
  text: 'Love it',
  url: '',
  hashtag: '',
  mention: '',
  image: 'FSTKkL_aIAAhsyh.jpeg',
  viewBox: '0 15 100 100',
  time: '7:27 pm, 9 May 2022',
  likes: 325,
  retweets: 50,
}

export const biodiverse: Tweet = {
  id: '1523550396291026944',
  avatar: 'tZKCyXmh_400x400.jpg',
  displayName: 'andy, ann takamaki\'s pig wrangler',
  userName: 'meltedmasks',
  text: '',
  url: '',
  hashtag: '',
  mention: '',
  image: 'FSS8_7lVgAAJ_J4.jpeg',
  viewBox: '15 0 75 75',
  time: '6:28 pm, 9 May 2022',
  likes: 436,
  retweets: 65,
}

export const intergenerational: Tweet = {
  id: '1523585803976253441',
  avatar: '1dgvYsDU_400x400.jpg',
  displayName: 'Alanna Irving',
  userName: 'alannairving',
  text: 'Reclaim our public space from these housing crisis profiteers. ',
  url: 'https://loweand.co.nz ',
  hashtag: '#HowLoweCanYouGo',
  mention: '',
  image: 'FSTdDQSVcAIcFnS.jpeg',
  viewBox: '18 0 75 75',
  time: '8:48 pm, 9 May 2022',
  likes: 86,
  retweets: 19,
}


export default [
  opportunity,
  flatmates,
  biodiverse,
  intergenerational,
  // residents,
  weOwnNewtown,
]